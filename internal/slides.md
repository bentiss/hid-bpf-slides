---
# try also 'default' to start simple
theme: seriph
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
#background: https://source.unsplash.com/collection/94734566/1920x1080
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# some information about the slides, markdown enabled
info: |
  ## HID-BPF presentation
  Presentation slides for developers.
# persist drawings in exports and build
drawings:
  persist: false
---

# HID-BPF

## mostly outdated

Benjamin Tissoires

---

# HID?

- Win 95 era protocol for handling plug and play USB devices (mice, keyboards)
  - now Bluetooth, BLE, I2C, Intel/AMD Sensors, (SPI in-progress)
- Most devices nowadays are working with generic drivers

<v-click>
<br>
<br>

### Except for a few of them that need:

</v-click>

<v-clicks>

  - a fixup in the report descriptor (45 drivers out of 82)
  - 41 files are under 100 LoC (counted with cloc)
  - some driver just change the input mapping (i.e. to enable a given key)


</v-clicks>

---

# HID: what it means to add a new quirk?

Device *x* is somewhat broken: a key is not properly reported:

<v-clicks>

- identification of the issue
- new patch created + tests
- submission on the LKML
- review of the patch
- inclusion in branch:
  - either scheduled for this cycle
  - either for the next (if big changes, like new driver)
- patch goes into Linus' tree
- kernel marked stable
- distributions take the new kernel
- RHEL backport?

</v-clicks>

<br>

<v-click>

Can take up to 6 months (12 for RHEL)

</v-click>


---

# BPF?

https://www.kernel.org/doc/html/latest/bpf/index.html

https://docs.cilium.io/en/latest/bpf/

> BPF is a highly flexible and efficient virtual machine-like construct in the Linux kernel allowing to execute bytecode at various hook points in a safe manner. It is used in a number of Linux kernel subsystems, most prominently networking, tracing and security (e.g. sandboxing).

<v-click>

Allows to add kernel space code from the user space (with root access).

</v-click>

---
layout: center
---

# HID-BPF

<br>

Use BPF in HID drivers to have user-space drivers in the kernel

---

# HID-BPF: the net-like capability

Change the incoming data flow

BPF program, compiled by clang:

```c
SEC("hid/raw_event")
int hid_x_event(struct hid_bpf_ctx *ctx)
{
	s16 *x = (s16*)&ctx->event.data[1];

	/* invert X coordinate */
	*x *= -1;

	return 0;
}
```

A program is attached to a `struct hid_device` in the kernel, by using the sysfs to attach to it (to be triggered by udev):

```bash
sudo ./hid_mouse /sys/bus/hid/devices/0018:06CB:CD7A.000A/modalias
```

Note: this is executed *before* `hidraw` or any driver processing.

---

# HID-BPF: Change the incoming data flow

SEC?

```c {1}
SEC("hid/raw_event")
int hid_x_event(struct hid_bpf_ctx *ctx)
{
	s16 *x = (s16*)&ctx->event.data[1];
	*x *= -1;
	return 0;
}
```

`tools/lib/bpf/bpf_helpers.h`:

```c
#define SEC(name) __attribute__((section(name), used))
```

Puts the defined object in the given ELF section.

Used by `libbpf` to convert the function into a BPF program type.

---
layout: two-cols
---

# HID-BPF: Change the incoming data flow


##### `include/uapi/linux/bpf_hid.h`:

```c
enum hid_bpf_event {
	/* ... */
};

struct hid_bpf_ctx {
	enum hid_bpf_event type;
	/* ... */
	struct {
		__u8 data[HID_BPF_MAX_BUFFER_SIZE];
		/* ... */
	} event;
};
```

::right::

##### BPF kernel program:

```c {2,4-5}
SEC("hid/raw_event")
int hid_x_event(struct hid_bpf_ctx *ctx)
{
	s16 *x = (s16*)&ctx->event.data[1];
	*x *= -1;
	return 0;
}
```

The BPF verifier ensure we only read and/or write at authorized offset in the context.

---

# HID-BPF: Load more than 1 program for `raw_event`

```c
SEC("hid/raw_event")
int hid_x_event(struct hid_bpf_ctx *ctx)
{
	s16 *x = (s16*)&ctx->event.data[1];
	*x *= -1;
	return 0;
}

SEC("hid/raw_event")
int hid_y_event(struct hid_bpf_ctx *ctx)
{
	s16 *y = (s16*)&ctx->event.data[3];
	*y *= -1;
	return 0;
}
```

Ordering of execution is implementation detail right now.

---

# HID-BPF: `raw_event`

Benefits/Use cases:
- Useful for neutral zone of a joystick
- Filter out unwanted fields in a stream
- Fix the report when something should not happen

---

# HID-BPF: changing how the device looks and talks

```c {all|1|4-6|all}
SEC("hid/rdesc_fixup")
int hid_rdesc_fixup(struct hid_bpf_ctx *ctx)
{
	/* invert X and Y definitions in the event stream interpretation */
	ctx->event.data[39] = 0x31;
	ctx->event.data[41] = 0x30;

	return 0;
}
```

`ctx->event.data` now contains the report descriptor of the device.

(Un)loading this program triggers a disconnect/reconnect of the device.

Only 1 program of this type per HID device.

---

# HID-BPF: `rdesc_fixup`

Benefits/Use cases:
- Fix a bogus report descriptor (key not properly mapped)
- Morph a device into something else (Surface Dial into a mouse)
- Change the device language (in conjunction with `raw_event`)
- Trigger reconnect to trace events


---

# HID-BPF: react to kernel/hw events

`PROBED, REMOVE, SUSPEND, RESUME, RESET_RESUME`

```c {all|1|7,8|12-16|all}
SEC("hid/event")
int set_haptic(struct hid_bpf_ctx *ctx)
{
	u8 *buf;
	/* ... */

	if (ctx->type != HID_BPF_PROBED)
		return 0;

	/* ... */

	buf[0] = 1;  /* report ID */
	bpf_hid_raw_request(ctx, buf, buflen, HID_FEATURE_REPORT, HID_REQ_GET_REPORT);

	buf[4] = 3;  /* haptic Auto Trigger */
	bpf_hid_raw_request(ctx, buf, buflen, HID_FEATURE_REPORT, HID_REQ_SET_REPORT);

	/* ... */

	return 0;
}
```

---

# HID-BPF: communicate with the device

`bpf_hid_raw_request()`

Same behavior than the in-kernel function `hid_hw_raw_request()`.

*Can not be used in interrupt context.*

<br>

Allows:
- query device information
- put the device into a specific mode

---

# Side note: memory management with eBPF

There is no `malloc`

```c
#include <bpf/bpf_helpers.h>

struct {
	__uint(type, BPF_MAP_TYPE_RINGBUF);
	__uint(max_entries, 4096 * 64);
} ringbuf SEC(".maps");

SEC("sth")
void my_function() {
	u8 *buf;
	const size_t buflen = 50 * sizeof(u8);

	buf = bpf_ringbuf_reserve(&ringbuf, buflen, 0);
	if (!buf)
		return 0;

	/* ... */

	bpf_ringbuf_discard(buf, 0);
}
```

---

# HID-BPF: Summary

- allow to add user-space defined behavior
- can add traces in the events
- can force re-probe of a device (`hid/rdesc_fixup`)
- will allow to live-fix devices without having to update the kernel
- no more custom kernel API (sysfs, module parameters)
- will **not** replace in-kernel drivers for devices broken at boot time (keyboards)

---
layout: center
---

# END

---

# Extra slide: HID-BPF: understand the device

```c {all|10-20|1,16|6-7}
static void item_cb(struct hid_bpf_ctx *ctx, struct hid_item *item, unsigned int idx)
{
	if (item->type == HID_ITEM_TYPE_GLOBAL &&
	    item->tag == HID_GLOBAL_ITEM_TAG_LOGICAL_MAXIMUM &&
	    item->data.u32 == 3600)
		/* Change Resolution Multiplier */
		bpf_hid_set_data(ctx, (idx + 1) << 3, item->size << 3, NEW_RES);
}

SEC("hid/rdesc_fixup")
int hid_rdesc_fixup(struct hid_bpf_ctx *ctx)
{
	struct hid_item *item;

	item = bpf_ringbuf_reserve(&ringbuf, sizeof(*item), 0);
	bpf_hid_foreach_rdesc_item(ctx, item, sizeof(*item), item_cb);
	bpf_ringbuf_discard(item, 0);

	return 0;
}
```

---

# HID-BPF: `bpf_hid_set_data()`

```c {6-9}
static void item_cb(struct hid_bpf_ctx *ctx, struct hid_item *item, unsigned int idx)
{
	if (item->type == HID_ITEM_TYPE_GLOBAL &&
	    item->tag == HID_GLOBAL_ITEM_TAG_LOGICAL_MAXIMUM &&
	    item->data.u32 == 3600)
		/* Change Resolution Multiplier */
		bpf_hid_set_data(ctx, (idx + 1) << 3, item->size << 3, NEW_RES);
}
```

`bpf_hid_set_data(ctx, offset_in_bits, size_in_bits, value)`

Helper to set a given value inside the `data` field of the context:

### Reason:

BPF prevents an array access from a variable.

