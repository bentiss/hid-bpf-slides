---
# try also 'default' to start simple
theme: seriph
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
#background: https://source.unsplash.com/collection/94734566/1920x1080
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: true
# some information about the slides, markdown enabled
info: |
  ## HID-BPF presentation
  Presentation slides for developers.
# persist drawings in exports and build
drawings:
  persist: false
layout: cover
---

# HID-BPF for eBPF developers

Benjamin Tissoires<br>
Red Hat

<br>
benjamin.tissoires@{redhat|gmail}.com

---

# HID?

- Human Interface Devices
- Win 95 era protocol for handling plug and play USB devices (mice, keyboards)
  - now Bluetooth, BLE, I2C, Intel/AMD Sensors, (SPI in-progress)
- Most devices nowadays are working with generic drivers

<v-click>
<br>
<br>

### Except for a few of them that need:

</v-click>

<v-clicks>

  - a fixup in the report descriptor (45 drivers out of 82)
    - `hid-sigmamicro.c` in v5.17
  - 41 files are under 100 LoC (counted with cloc)
  - some driver just change the input mapping (i.e. to enable a given key)
    - `hid-razer` in v5.17


</v-clicks>

<v-click>

<br>

### Can eBPF help?

</v-click>

---
layout: center
---

# HID, a Plug & Play protocol

---

# Documentation

- [Device Class Definition](https://www.usb.org/document-library/device-class-definition-hid-111)
- [HID Usage Tables](https://www.usb.org/document-library/hid-usage-tables-13)

---

# Device Class Definition

https://www.usb.org/document-library/device-class-definition-hid-111

- there are the equivalent files for I2C, Bluetooth, BLE, SPI
- last update: May 27, 2001
- defines generic protocol that every HID device must speak
  - operational model
  - descriptors (USB + HID report descriptor)
  - parser of report descriptors
  - requests
  - report protocol

---

# HID Usage Tables

https://www.usb.org/document-library/hid-usage-tables-13

- last update: April 5, 2021
- defines *meaning* of usages as defined in the report descriptor
  - X and Y are defined in the Generic Desktop page (0x01) as 0x30 and 0x31
- can be extended (and is) by companies
  - multitouch protocol
  - USI pens
  - HW sensors
- except for a few exceptions: an update means a new `#define` in the kernel if we care

---

# HID report descriptor

- describes the device protocol
- "simple" language (no loops, conditionals, etc...)

```c {all|1-3|4-5|6-10|11,12|13-19|20,21}
0x05, 0x01,       // Usage Page (Generic Desktop)
0x09, 0x02,       // Usage (Mouse)
0xa1, 0x01,       // Collection (Application)             <-- Application(Mouse)
0x09, 0x01,       //  Usage (Pointer)
0xa1, 0x00,       //  Collection (Physical)               <-- Physical(Pointer)
0x05, 0x09,       //   Usage Page (Button)
0x15, 0x00, 0x25, 0x01, 0x19, 0x01, 0x29, 0x05,  //   Logical Min/Max and Usage Min/Max
0x75, 0x01,       //   Report Size (1)                    <- each usage is 1 bit
0x95, 0x05,       //   Report Count (5)                   <- we got 5 of them
0x81, 0x02,       //   *Input* (Data,Var,Abs)      <--- 5 bits for 5 buttons
0x95, 0x03,       //   Report Count (3)
0x81, 0x01,       //   *Input* (Cnst,Arr,Abs)      <--- 3 bits of padding
0x05, 0x01,       //   Usage Page (Generic Desktop)
0x16, 0x01, 0x80, 0x26, 0xff, 0x7f, //   Logical Min/Max
0x09, 0x30,       //   Usage (X)
0x09, 0x31,       //   Usage (Y)
0x75, 0x10,       //   Report Size (16)
0x95, 0x02,       //   Report Count (2)
0x81, 0x06,       //   *Input* (Data,Var,Rel)      <--- X,Y of 16 bits
0x15, 0x81, 0x25, 0x7f,   //   Logical Min/Max (-127,127)
0x09, 0x38,       //   Usage (Wheel)
0x75, 0x08,       //   Report Size (8)
0x95, 0x01,       //   Report Count (1)
0x81, 0x06,       //   *Input* (Data,Var,Rel)      <--- Wheel of 8 bits
0x05, 0x0c,       //   Usage Page (Consumer Devices)
0x0a, 0x38, 0x02, //   Usage (AC Pan)
0x95, 0x01,       //   Report Count (1)
0x81, 0x06,       //   *Input* (Data,Var,Rel)      <--- AC Pan of 8 bits
0xc0,             //  End Collection
0xc0,             // End Collection
```
---
layout: center
---

# HID, a not so simple world

---

# HID, a not so simple world

few examples:
- Cirque touchpad which lies on to their physical specifications
- Sigma Micro keyboards that needs a fixup
- Razer keyboards which do not enable keys at boot
- Microsoft Surface Dial which is not usable without faking it as a mouse

---

# HID: what it means to add a new quirk?

Device *x* is somewhat broken: a key is not properly reported:

<v-clicks>

- identification of the issue
- new patch created + tests
- user needs to recompile the kernel
- submission on the LKML
- review of the patch
- inclusion in branch:
  - either scheduled for this cycle
  - either for the next (if big changes, like new driver)
- patch goes into Linus' tree
- kernel marked stable or patch backported in stable
- distributions take the new kernel
- user can drop the custom kernel build

</v-clicks>

---

# HID: what it means to add a new quirk with BPF?

Device *x* is somewhat broken: a key is not properly reported:

- identification of the issue
- new ~~patch~~ *BPF program* created + tests
- user ~~needs to recompile the kernel~~ drops the bpf program into the filesystem

Users stop here once the BPF program is accepted.

- submission on the LKML
- review of the patch with *the bpf program*
- inclusion in branch
- patch goes into Linus' tree
- kernel marked stable or patch backported in stable
- distributions take the new kernel
- ~~user can drop the custom kernel build~~


---
layout: center
---

# HID-BPF

<br>

Use BPF in HID drivers to have user-space drivers in the kernel

---

# HID-BPF: base principles

- works only on **arrays of bytes** and talks HID
  - bit access is allowed for length <= 32
  - no access to input, or any other subsystems (LEDs, force feedback, ...)
- any *smart* processing needs to be done in userspace:
  - parse HID report descriptor
  - compute location of various fields

- programs needs to be CORE (like)
  - users should not be required to have LLVM
- best if we can target a specific device for a given program
- need to enforce GPL programs

---

# HID-BPF: the net-like capability

Change the incoming data flow

BPF program, compiled by clang:

```c
SEC("hid/device_event")
int hid_x_event(struct hid_bpf_ctx *ctx)
{
	__s16 *x = (__s16*)bpf_hid_get_data(ctx, 1, 2);

	/* invert X coordinate */
	*x *= -1;

	return 0;
}
```

A program is attached to a `struct hid_device` in the kernel, by using the sysfs to attach to it (to be triggered by udev):

```bash
sudo ./hid_mouse /sys/bus/hid/devices/0018:06CB:CD7A.000A/uevent
```

Note: this is executed *before* `hidraw` or any driver processing.

---

# HID-BPF: Load more than 1 program for `device_event`

```c
SEC("hid/device_event")
int hid_x_event(struct hid_bpf_ctx *ctx)
{
	__s16 *x = (__s16*)bpf_hid_get_data(ctx, 1, 2);
	*x *= -1;
	return 0;
}

SEC("hid/device_event")
int hid_y_event(struct hid_bpf_ctx *ctx)
{
	__s16 *y = (__s16*)bpf_hid_get_data(ctx, 3, 2);
	*y *= -1;
	return 0;
}
```

Ordering of execution is implementation detail right now.

---

# HID-BPF: `device_event`

Benefits/Use cases:
- Useful for neutral zone of a joystick
- Filter out unwanted fields in a stream
- Fix the report when something should not happen

---

# HID-BPF: changing how the device looks and talks

```c
SEC("hid/rdesc_fixup")
int hid_rdesc_fixup(struct hid_bpf_ctx *ctx)
{
	__u8 *data = bpf_hid_get_data(ctx, 0 /* offset */, 4096 /* size */);

	/* invert X and Y definitions in the event stream interpretation */
	data[39] = 0x31;
	data[41] = 0x30;

	return 0;
}
```

`data` now contains the report descriptor of the device.

(Un)loading this program triggers a disconnect/reconnect of the device.

Only 1 program of this type per HID device.

---

# HID-BPF: `rdesc_fixup`

Benefits/Use cases:
- Fix a bogus report descriptor (key not properly mapped)
- Morph a device into something else (Surface Dial into a mouse)
- Change the device language (in conjunction with `device_event`)
- *Trigger reconnect to trace events*


---

# HID-BPF: react to kernel/hw events

`PROBED, REMOVE, SUSPEND, RESUME, RESET_RESUME`

```c {all|1-8|10-22}
SEC("hid/driver_event")
int set_haptic(struct hid_bpf_ctx *ctx)
{
	if (ctx->type != HID_BPF_PROBED)
		return 0;
	/* notify userspace with a perf event */
	return 0;
}

SEC("hid/user_event")
int set_haptic(struct hid_bpf_ctx *ctx)
{
	__u8 *buf;
	/* ... */
	buf[0] = 1;  /* report ID */
	bpf_hid_raw_request(ctx, buf, buflen, HID_FEATURE_REPORT, HID_REQ_GET_REPORT);

	buf[4] = 3;  /* haptic Auto Trigger */
	bpf_hid_raw_request(ctx, buf, buflen, HID_FEATURE_REPORT, HID_REQ_SET_REPORT);
	/* ... */
	return 0;
}
```

---

# HID-BPF: communicate with the device

`bpf_hid_raw_request()`

Same behavior than the in-kernel function `hid_hw_raw_request()`.

*Can not be used in interrupt context.*

<br>

Allows:
- query device information
- put the device into a specific mode

---

# Current patch series

- HID/BPF integration is getting refined
  - messy because hid.ko
- access to data through `bpf_hid_get_data()`
- `SEC("hid/device_event")` done IMO
- `SEC("hid/rdesc_fixup")` done IMO
- `SEC("hid/user_event")` done IMO
- `SEC("hid/driver_event")`:
  - base type declared
  - need various types and HID implementation (no BPF implication IMO)
    - suspend/resume, probed/removed, pre/post raw_request

---

# HID-BPF: future

- finish `SEC("hid/driver_event")` subtypes
  - to implement firewall-like capabilities
- might need a `bpf_hid_inject_event()` at some point
  - useful for macro keys
- add autoloading mechanism of in-kernel BPF programs
  - ideally, just drop the bpf source in the tree and it gets automagically included

- rewrite using Alexei's suggestion of using `fmod_ret`?

---

# HID-BPF: Summary

- should simplify easy fixes in the future
- allow to add user-space defined behavior
- can add traces in the events
- will allow to live-fix devices without having to update the kernel
- no more custom kernel API (sysfs, module parameters)
- will **not** replace in-kernel drivers for devices broken at boot time (keyboards)

---
layout: center
---

# END

